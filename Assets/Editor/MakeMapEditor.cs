﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MakeMap))]
public class MakeMapEditor : Editor
{
    private bool m_cube = false, m_clear = false;
    private int m_sizeW = 22, m_sizeH = 37;

    public override void OnInspectorGUI()
    {
        MakeMap map = (MakeMap)target;

        GUILayout.Label("Item Generator");

        m_sizeW = EditorGUILayout.IntField(m_sizeW);
        m_sizeH = EditorGUILayout.IntField(m_sizeH);

        //m_cube = GUILayout.Button("Make");
        if (m_cube)
        {
            GameObject cube = Resources.Load<GameObject>("floor_plain");
            GameObject aliveCube;


            for (int w = 0; w < m_sizeW; w++)
            {
                for (int h = 0; h < m_sizeH; h++)
                {
                    float x = w + map.transform.position.x;
                    float z = h + map.transform.position.z;
                    aliveCube = Instantiate(cube, new Vector3(x, map.transform.position.y, z), Quaternion.identity, map.transform);
                    aliveCube.name = w + " " + h;
                }
            }
        }

        //m_clear = GUILayout.Button("Clear (spam me)");
        if (m_clear)
        {
            KillChildern(map);
        }

        base.OnInspectorGUI();
    }

    private void KillChildern(MakeMap @object)
    {
        for (int i = 0; i < @object.transform.childCount; i++)
        {
            Transform trans = @object.transform.GetChild(i);
            DestroyImmediate(trans.gameObject);
        }
    }
}
