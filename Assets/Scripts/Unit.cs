﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Animation
{
    STILL,
    SHOOT,
    DEAD,
    WALK
}

[RequireComponent(typeof(Collider))]
public class Unit : MonoBehaviour
{
    private SkinnedMeshRenderer m_renderer;
    private Material m_materialDefault;
    private MakeMap m_map;
    private Gamemanager m_game;
    private UnitSelectorShoot m_selector;
    private Animator m_animator;
    public const string BLEND = "Blend";
    private Shooting m_shooting;
    [SerializeField] private bool m_active;

    [SerializeField] private UnitType m_type;
    [SerializeField] private Vector3 m_mouseInWorld;
    [SerializeField] private bool m_selected;
    [SerializeField] private Material m_materialSelected;

    public UnitType Type
    {
        get
        {
            return m_type;
        }
    }

    public Animator Animator
    {
        get
        {
            return m_animator;
        }
    }

    private void Start()
    {
        m_renderer = transform.GetChild(1).GetComponent<SkinnedMeshRenderer>();
        m_materialDefault = m_renderer.material;
        m_game = GameObject.Find("GameManager").GetComponent<Gamemanager>();
        m_game.OnTurnStartEvent += OnStart;
        m_game.OnTurnStopEvent += OnStop;
        m_selector = m_game.GetComponent<UnitSelectorShoot>();
        m_animator = GetComponent<Animator>();
        m_shooting = GetComponent<Shooting>();

        m_active = true;

        if (m_type.range < 0) Debug.LogWarning("Speed is fucked");

        GetComponent<Collider>().enabled = false; //just in case
    }

    public void Init()
    {
        m_map = transform.parent.GetComponent<Cell>().Map;
    }

    private void OnMouseDown()
    {
        m_selected = !m_selected;

        if (m_active == false)
        {
            m_selected = false;
        }
        else
        {
            if (m_selected && !m_shooting.IsDead)
            {
                ShowMoveOptions(CellState.WALK);
                m_selector.AddMeOrigin(this, m_type.team);
            }
            else
            {
                ShowMoveOptions(CellState.IDEL);
                
            }
        }

        m_selector.AddMeTarget(this, m_type.name);
    }

    private void OnMouseOver()
    {
        m_renderer.material = m_materialSelected;
    }

    private void OnMouseExit()
    {
        m_renderer.material = m_materialDefault;
    }

    private void ShowMoveOptions(CellState state)
    {
        if(transform.parent == null)
        {
            Debug.Log("Unit test mode");
            return;
        }
        string[] words = transform.parent.name.Split(' ');
        float w = float.Parse(words[0], System.Globalization.CultureInfo.InvariantCulture);
        float h = float.Parse(words[1], System.Globalization.CultureInfo.InvariantCulture);

        int r = (int)m_type.range;

        for (int x = -r; x < r + 1; x++)
        {
            int height = (int)Mathf.Sqrt(r * r - x * x);

            for (int y = -height; y < height + 1; y++)
            {
                float ww = x + w;
                float hh = y + h;
                Transform t = m_map.GetCell(ww + " " + hh);
                if (t != null) { t.GetComponent<Cell>().SetState(state, this); }
            }
        }
    }

    public void OnStart(string team)
    {
        if (m_type.team == team)
        {
            m_active = true;
        }
    }

    public void OnStop(string team)
    {
        if (m_type.team == team)
        {
            m_active = false;
        }
        ShowMoveOptions(CellState.IDEL);
    }
}
