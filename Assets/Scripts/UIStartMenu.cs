﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIStartMenu : MonoBehaviour {

    [SerializeField] private int m_gameScene;
    private AudioSource m_audio;

    private void Start()
    {
        m_audio = GetComponent<AudioSource>();
    }

    public void OnStart()
    {
        m_audio.Play();
        SceneManager.LoadScene(m_gameScene);
    }

    public void OnQuit()
    {
        m_audio.Play();
        Application.Quit();
    }
}
