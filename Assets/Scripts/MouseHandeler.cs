﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseHandeler : MonoBehaviour
{

    [SerializeField]
    private Vector3 m_mouseInWorld;
    [SerializeField]
    private Transform m_pointer;
    [SerializeField]
    private LayerMask m_layer;
    private bool m_holding;
    private Transform m_placeholder;
    [SerializeField]
    private MakeMap m_map;
    private Camera m_camera;
    private AudioSource m_audio;
    [SerializeField] private AudioClip m_buttonClick;
    [SerializeField] private AudioClip m_placeUnit;

    [Header("Red prefabs")]
    [SerializeField] private GameObject m_redRifel;
    [SerializeField] private GameObject m_redHavy;
    [SerializeField] private GameObject m_redScout;
    [Header("Blue prefabs")]
    [SerializeField] private GameObject m_blueRifel;
    [SerializeField] private GameObject m_blueHavy;
    [SerializeField] private GameObject m_blueScout;

    public Camera Camera
    {
        get
        {
            return m_camera;
        }

        set
        {
            m_camera = value;
        }
    }

    private void Start()
    {
        m_audio = GetComponent<AudioSource>();
        m_placeholder = m_pointer;
    }

    private void FixedUpdate()
    {
        if (m_camera != null)
        {
            Ray ray = m_camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, m_layer))
            {
                Debug.DrawLine(ray.origin, hit.point);
                m_mouseInWorld = hit.point;
            }

            m_placeholder.position = m_mouseInWorld;
        }
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && m_holding)
        {
            Unit unit = m_placeholder.GetComponent<Unit>();
            bool isBaseCell = m_map.SelectedCell.GetComponent<Cell>().GetIsBaseCell(unit.Type.team);

            if (!m_map.SelectedCell)
            {
                //Debug.LogError("U forgot the Cell script");
            }
            else if (m_map.SelectedCell.transform.childCount == 0 && isBaseCell)
            {
                Collider collider = m_placeholder.GetComponent<Collider>();
                

                m_placeholder.parent = m_map.SelectedCell.transform;
                float x = m_map.SelectedCell.transform.position.x;
                float y = 1f;
                float z = m_map.SelectedCell.transform.position.z;
                m_placeholder.position = new Vector3(x, y, z);
                m_placeholder.transform.localPosition = new Vector3(-0.5f, 0, -0.5f);

                unit.Init();

                m_placeholder = m_pointer;
                m_holding = false;
                collider.enabled = true;

                m_audio.clip = m_placeUnit;
                m_audio.Play();
            }
        }
        else if (Input.GetMouseButtonDown(0) && m_holding)
        {

        }
    }

    public void OnRedRifelUnit()
    {
        Spawn(m_redRifel);
    }
    public void OnRedHavyUnit()
    {
        Spawn(m_redHavy);
    }

    public void OnRedScoutUnit()
    {
        Spawn(m_redScout);
    }

    public void OnBlueRifelUnit()
    {
        Spawn(m_blueRifel);
    }
    public void OnBlueHavyUnit()
    {
        Spawn(m_blueHavy);
    }

    public void OnBlueScoutUnit()
    {
        Spawn(m_blueScout);
    }

    private void Spawn(GameObject unit)
    {
        if(m_holding == true) //prevents from holding more than one unit
        {
            return;
        }

        m_audio.clip = m_buttonClick;
        m_audio.Play();
        GameObject go = Instantiate(unit);
        m_placeholder = go.transform;
        m_holding = true;
    }
}
