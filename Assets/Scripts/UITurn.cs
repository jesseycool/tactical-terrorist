﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITurn : MonoBehaviour
{

    [SerializeField] private Gamemanager m_game;
    [SerializeField] private GameObject m_redGUI;
    [SerializeField] private GameObject m_blueGUI;

    private void Start()
    {
        m_game.OnTurnStartEvent += OnStart;
    }

    public void OnStart(string team)
    {
        if (team.Equals("red"))
        {
            m_redGUI.SetActive(true);
            m_blueGUI.SetActive(false);
        }
        else if (team.Equals("blue"))
        {
            m_redGUI.SetActive(false);
            m_blueGUI.SetActive(true);
        }
    }
}
