﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeMap : MonoBehaviour
{

    [SerializeField] private GameObject m_selectedCell;
    private AudioSource m_audio;
    [SerializeField] private AudioClip m_placeUnit;

    private void Start()
    {
        m_audio = GetComponent<AudioSource>();
    }

    public GameObject SelectedCell
    {
        get
        {
            return m_selectedCell;
        }

        set
        {
            m_selectedCell = value;
        }
    }

    public Transform GetCell(string cords)
    {
        Transform hotseat = null;
        for (int i = 0; i < transform.childCount; i++)
        {
            string name = transform.GetChild(i).name;
            if (name == cords)
            {
                hotseat = transform.GetChild(i);
            }
        }
        return hotseat;
    }

    public void Clear()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).GetComponent<Cell>().SetState(CellState.IDEL, null);
        }
    }

    public void PlayPlaceSound()
    {
        m_audio.clip = m_placeUnit;
        m_audio.Play();
    }
}
