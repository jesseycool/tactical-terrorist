﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AttackType
{
    RIFEL,
    SCOUT,
    HAVY
}

[RequireComponent(typeof(Unit))]
[RequireComponent(typeof(AudioSource))]
public class Shooting : MonoBehaviour
{
    float max_health;
    [SerializeField] float health;
    float health_min = 0;
    private bool m_isDead = false;
    private Unit m_unit;
    private Vector3 m_lineDirection;
    private AudioSource m_audio;
    private Unit m_target;
    [SerializeField] private AudioClip[] m_soundLibary;

    public float Health
    {
        get
        {
            return health;
        }
    }

    public bool IsDead
    {
        get
        {
            return m_isDead;
        }
    }

    // Use this for initialization
    void Start()
    {
        m_audio = GetComponent<AudioSource>();
        m_unit = GetComponent<Unit>();
        max_health = m_unit.Type.max_health;

        health = max_health;
    }

    // Update is called once per frame
    void Update()
    {
        if (health < 0)
        {
            health = health_min;
            m_isDead = true;
            m_unit.Animator.SetFloat(Unit.BLEND, (int)Animation.DEAD);
        }
    }

    public void Shoot(AttackType type, Vector3 lineDirection, Unit target)
    {
        m_lineDirection = lineDirection;
        m_target = target;
        switch (type)
        {
            case AttackType.RIFEL:
                // Rifle man
                ShootAction(10, 8);
                ShootAction(15, 5);
                ShootAction(20, 3);
                PlayId(0);
                break;
            case AttackType.SCOUT:
                // Squad
                for (int i = 1; i < 7; i++)
                {
                    ShootAction(10, 7);
                }
                PlayId(1);
                break;
            case AttackType.HAVY:
                // Riot shield
                ShootAction(20, 8);
                PlayId(2);
                break;
        }
        m_unit.Animator.SetFloat(Unit.BLEND, (float)Animation.SHOOT);
    }

    private void ShootAction(float damage, int chance)
    {
        int hit_chance = Random.Range(0, 11);
        if (hit_chance < chance)
        {
            Shooting opponent = m_target.transform.GetComponent<Shooting>();
            if (opponent != null) { opponent.damage(damage); }
        }
    }

    //private void ShootAction(float damage, int chance)
    //{
    //    int hit_chance = Random.Range(0, 11);
    //    if (hit_chance < chance)
    //    {
    //        Vector3 fwd = m_lineDirection; //transform.forward;
    //        RaycastHit hit;
    //        if (Physics.Raycast(transform.position, fwd, out hit, Mathf.Infinity))
    //        {
    //            print("It hit " + hit.collider.name);
    //            Shooting opponent = hit.transform.GetComponent<Shooting>();
    //            if (opponent != null) { opponent.damage(damage); }
    //        }
    //        Debug.DrawRay(transform.position, fwd * range);
    //    }
    //}
    public void damage(float amount)
    {
        health = health - amount;
        if (m_unit.Type.attackType == AttackType.HAVY)
        {
            PlayId(3);
        }
    }

    private void PlayId(int id)
    {
        m_audio.clip = m_soundLibary[id];
        m_audio.Play();
    }
}
