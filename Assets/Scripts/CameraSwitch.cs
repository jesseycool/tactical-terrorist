﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitch : MonoBehaviour {

    [SerializeField] private Gamemanager m_game;
    [SerializeField] private MouseHandeler m_mouse;
    private GameObject m_redCamOb;
    private GameObject m_blueCamOb;
    private Camera m_redCam;
    private Camera m_blueCam;

    private void Start()
    {
        m_redCamOb = transform.GetChild(0).gameObject;
        m_blueCamOb = transform.GetChild(1).gameObject;
        m_redCam = m_redCamOb.transform.GetChild(0).GetComponent<Camera>();
        m_blueCam = m_blueCamOb.transform.GetChild(0).GetComponent<Camera>();

        m_game.OnTurnStartEvent += OnStart;
    }

    public void OnStart(string team)
    {
        if (team.Equals("red"))
        {
            m_mouse.Camera = m_redCam;
            m_redCamOb.SetActive(true);
            m_blueCamOb.SetActive(false);
        }
        else if (team.Equals("blue"))
        {
            m_mouse.Camera = m_blueCam;
            m_redCamOb.SetActive(false);
            m_blueCamOb.SetActive(true);
        }
    }
}
