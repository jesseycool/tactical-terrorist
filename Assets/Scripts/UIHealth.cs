﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIHealth : MonoBehaviour {

    private Shooting m_health;
    private GameObject m_block;
    private UnitType m_type;

    private void Start () {
        m_health = transform.parent.GetComponent<Shooting>();
        m_type = transform.parent.GetComponent<Unit>().Type;

        if (m_type.team == "blue") { m_block = Resources.Load<GameObject>("helf_0"); }
        else if (m_type.team == "red") { m_block = Resources.Load<GameObject>("helf_1"); }
    }

    private void FixedUpdate()
    {
        MakeMe();
    }

    private void MakeMe () {

        for (int i = 0; i < transform.childCount; i++)
        {
            Transform trans = transform.GetChild(i);
            Destroy(trans.gameObject);
        }

        float blockCount = m_health.Health / 10;

        for (int i = 0; i < (int)blockCount; i++)
        {
            GameObject go = Instantiate(m_block);
            go.transform.parent = transform;

            go.transform.localPosition = new Vector3(-(i * 0.5f), 0, 0);
        }

        Vector3 vector = new Vector3(blockCount * 0.25f, transform.localPosition.y, transform.localPosition.z);
        transform.localPosition = vector;

    }
}
