﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CellState
{
    IDEL,
    HOVER,
    WALK
}

public class Cell : MonoBehaviour
{

    private MeshRenderer m_renderer;
    private MakeMap m_map;
    private Unit m_origin;
    [SerializeField] private Material m_materialDefault;
    [SerializeField] private Material m_materialHover;
    [SerializeField] private Material m_materialWalk;
    [SerializeField] private CellState m_state = CellState.IDEL;
    [SerializeField] private bool m_isRedBaseCell;
    [SerializeField] private bool m_isBlueBaseCell;

    public MakeMap Map
    {
        get
        {
            return m_map;
        }
    }

    private void Start()
    {
        m_renderer = GetComponent<MeshRenderer>();
        m_map = GetComponentInParent<MakeMap>();
    }

    private void FixedUpdate()
    {
        switch (m_state)
        {
            case CellState.IDEL:
                m_renderer.material = m_materialDefault;
                break;
            case CellState.HOVER:
                if(m_isRedBaseCell || m_isBlueBaseCell) m_renderer.material = m_materialHover;
                break;
            case CellState.WALK:
                m_renderer.material = m_materialWalk;
                break;
        }
    }

    public void SetState(CellState state, Unit origin)
    {
        m_state = state;
        m_origin = origin;
    }

    public CellState GetState()
    {
        return m_state;
    }

    public bool GetIsBaseCell(string team)
    {
        if(team == "red")
        {
            return m_isRedBaseCell;
        }
        else if (team == "blue")
        {
            return m_isBlueBaseCell;
        }
        return false;
    }

    private void OnMouseOver()
    {
        if (m_state != CellState.WALK)
        {
            m_state = CellState.HOVER;
        }
        m_map.SelectedCell = this.gameObject;
    }

    private void OnMouseExit()
    {
        if (m_state != CellState.WALK)
        {
            m_state = CellState.IDEL;
        }
        m_map.SelectedCell = null;
    }

    private void OnMouseDown()
    {
        if (m_state == CellState.WALK)
        {
            m_origin.Animator.SetFloat(Unit.BLEND, (int)Animation.WALK);

            m_origin.transform.parent = transform;
            float x = transform.position.x;
            float y = 1f;
            float z = transform.position.z;
            m_origin.transform.position = new Vector3(x, y, z);
            
            m_origin.transform.localPosition = new Vector3(-0.5f, 0, -0.5f);

            m_map.Clear();
            m_map.PlayPlaceSound();
        }
    }
}
