﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitSelectorShoot : MonoBehaviour {

    private Unit m_attacker;
    private Unit m_target;
    private string m_attackingTeam;

    public void AddMeOrigin(Unit unit, string team)
    {
        m_attacker = unit;
        m_attackingTeam = team;
    }

    public void AddMeTarget(Unit unit, string team)
    {
        if (m_attackingTeam != team)
        {
            m_target = unit;
            Attack();
        }
    }

    private void Attack()
    {
        if (m_attacker != null && m_target != null) //null check
        {
            if (m_attacker.Type.team != m_target.Type.team) //if difernd team
            {
                if (m_target.transform.parent.GetComponent<Cell>().GetState() == CellState.WALK) //if in range
                {
                    Debug.Log("attack");
                    AttackType attackType = m_attacker.Type.attackType;
                    Shooting gun = m_attacker.GetComponent<Shooting>();
                    Vector3 heading = m_target.transform.position - m_attacker.transform.position;
                    var distance = heading.magnitude;
                    var direction = heading / distance; //norm

                    gun.Shoot(attackType, direction, m_target);
                }
            }
        }
    }
}
