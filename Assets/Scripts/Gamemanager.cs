﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gamemanager : MonoBehaviour
{

    public delegate void OnTurnStartDelegate(string team);
    public event OnTurnStartDelegate OnTurnStartEvent;

    public delegate void OnTurnStopDelegate(string team);
    public event OnTurnStopDelegate OnTurnStopEvent;

    [SerializeField] private bool m_isRedTurn = false;
    [SerializeField] private bool m_isBlueTurn = true;
    private AudioSource m_audio;

    private void Start()
    {
        m_audio = GetComponent<AudioSource>();
        TurnInit();
    }

    public void ChangeTurn()
    {
        m_audio.Play();
        if (m_isRedTurn)
        {
            if (OnTurnStopEvent != null) OnTurnStopEvent("blue"); //stop blue
            if (OnTurnStartEvent != null) OnTurnStartEvent("red"); //start red
        }

        if (m_isBlueTurn)
        {
            if (OnTurnStopEvent != null) OnTurnStopEvent("red"); //stop red
            if (OnTurnStartEvent != null) OnTurnStartEvent("blue"); //start blue
        }

        m_isRedTurn = !m_isRedTurn;
        m_isBlueTurn = !m_isBlueTurn;
    }

    private void TurnInit()
    {
        if (m_isRedTurn)
        {
            if (OnTurnStopEvent != null) OnTurnStopEvent("blue"); //stop blue
            if (OnTurnStartEvent != null) OnTurnStartEvent("red"); //start red
        }

        if (m_isBlueTurn)
        {
            if (OnTurnStopEvent != null) OnTurnStopEvent("red"); //stop red
            if (OnTurnStartEvent != null) OnTurnStartEvent("blue"); //start blue
        }

        m_isRedTurn = !m_isRedTurn;
        m_isBlueTurn = !m_isBlueTurn;
    }
}
