﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "UnitType", menuName = "Inventory/UnitType", order = 1)]
public class UnitType : ScriptableObject
{

    public string team;
    public float max_health;
    public float range;
    public AttackType attackType;

}
